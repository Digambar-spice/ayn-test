import { HttpClient, HttpHeaders, HttpEventType, HttpErrorResponse } from '@angular/common/http';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest
} from '@angular/common/http';
import {environment} from '../../environments/environment'
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Observable } from "rxjs/Observable";
import "rxjs/add/operator/toPromise";
import "rxjs/add/operator/catch";
import "rxjs/observable/of";
import "rxjs/add/operator/share";
import "rxjs/add/operator/map";
@Injectable()
export class HttpClients {
  userid: any;
  rootUrl: string = environment.apiUrl;
  constructor(
    private http: HttpClient,
  ) {}

  get<T>(url: string): Observable<T> {
    const urls = this.handleUrl(url);
    // const data = this.http.get<T>(urls);
    return this.http.get<T>(urls);
    // return this.http.get<T>(urls).map(response => {
    //   this.encryptDecryptService.decrypt(JSON.stringify(response));
    // });
  }

  post<T>(url: string, body: string): Observable<T> {
    const urls = this.handleUrl(url);
    // const data = this.encrypt(JSON.stringify(body));    
    return this.http.post<T>(urls, body).map(response => response);
    // return this.http.post<T>(urls, data).map(response => {
    //     return this.encryptDecryptService.decrypt(JSON.stringify(response));
    // });
  }

  Post<T>(url: string, body: FormData): Observable<T> {
    const urls = this.handleUrl(url);
    // const data = this.encrypt(JSON.stringify(body));
    // return this.http.post<T>(urls, body).map(response => response);
    // return this.http.post<T>(urls, data).map(response => {
    //     return this.encryptDecryptService.decrypt(JSON.stringify(response));
    // });

    return this.http.post( urls, body, {
      reportProgress: true,
      observe: 'events'
    }).pipe(
      map(event => this.getEventMessage(event, body)),
      catchError(this.handleError)
    );
  }

  private getEventMessage(event: HttpEvent<any>, body) {

    switch (event.type) {
      case HttpEventType.Response:
        return this.apiResponse(event);
		break;
        case HttpEventType.UploadProgress:
        return this.fileUploadProgress(event)
		break;
      default:
        // return `File "${formData.get('profile').name}" surprising upload event: ${event.type}.`;
    }
  }
  private fileUploadProgress(event) {
    const percentDone = Math.round(100 * event.loaded / event.total);
    console.log('test',percentDone);
    return { status: 'progress', message: percentDone };
  }

  private apiResponse(event) {
    // console.log(event.body);
    return event.body;
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened. Please try again later.');
  }

  put<T>(url: string, body: string): Observable<T> {
    // const data = this.encrypt(JSON.stringify(body));
    return this.http.put<T>(url, body);
    // return this.http.put<T>(urls, body).map(response => {
    //   return this.encrypt(JSON.stringify(response));
    // });
  }

  delete<T>(url: string): Observable<T> {
    return this.http.delete<T>(url);
    // return this.http.delete<T>(urls).map(response => {
    //   return this.encrypt(JSON.stringify(response));
    // });
  }

  patch<T>(url: string, body: string): Observable<T> {
    return this.http.patch<T>(url, body);
  }

  private handleUrl(url: string): string {
    if (!this.checkUrlExternal(url)) {
      if (url.charAt(0) === '/') {
        url = url.substring(1);
      }
      url = this.rootUrl + url;
    }
    return url;
  }
  private checkUrlExternal(url: string): boolean {
    return /^(?:[a-z]+:)?\/\//i.test(url);
  }

/*   encrypt(data) {
    return this.encryptDecryptService.encrypt(data);
  }

  decrypt(data) {
    return this.encryptDecryptService.decrypt(data);
  } */
}