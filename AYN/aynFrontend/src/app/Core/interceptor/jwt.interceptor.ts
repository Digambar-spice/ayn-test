import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { HttpClients } from '../httpClient';
import { CommService } from '../../common-services/comm.service';
import 'rxjs/add/operator/do';
import { tap } from 'rxjs/operators';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';
import 'rxjs/observable/of';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/map';
import { interval } from 'rxjs';
import 'rxjs/add/observable/interval';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  sub: any;
  constructor(public commService: CommService, private HttpClient: HttpClients) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      request = request.clone({
      setHeaders: {
        Authorization: `Bearer${currentUser.token.Token}`,
        RefreshToken: `Bearer${currentUser.token.RefreshToken}`,
      }
      });
    }
    return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
      const currentUser = JSON.parse(localStorage.getItem('currentUser'));
      if (currentUser && currentUser.token) {
        request = request.clone({
        setHeaders: {
          Authorization: `Bearer${currentUser.token.Token}`,
          RefreshToken: `Bearer${currentUser.token.RefreshToken}`,
        }
        });
      }

      this.sub = interval(150000 * 60).subscribe(x => {
          this.refreshToken(request);
        });
    }));
  }

  refreshToken(request) {
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      request = request.clone({
      setHeaders: {
        Authorization: `Bearer${currentUser.token.Token}`,
        RefreshToken: `Bearer${currentUser.token.RefreshToken}`,
      }
      });
    }
    this.commService.getAuthorizationToken().subscribe((response: any) => {
      if (response) {
          const data = response;
          if (data.Token) {
            // Update tokens
            const user = {token : {
                Token : data.Token,
                RefreshToken: data.RefreshToken
            }};
            localStorage.setItem('currentUser', JSON.stringify(user));
            if (this.sub) {
              this.sub.unsubscribe();
            }
            // Clone our field request ant try to resend it
            request = request.clone({
              setHeaders: {
                Authorization: `Bearer${data.Token}`,
                RefreshToken: `Bearer${data.RefreshToken}`,
              }
            });
          } else {
            // Logout from account
            // this.authService.logout();
          }
      }
    });
  }
}
