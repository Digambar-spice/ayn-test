import { Component } from '@angular/core';
// import { HttpClients } from './Core/httpClient';
import { BnNgIdleService } from 'bn-ng-idle';
import { Observable, of, throwError } from 'rxjs';
import { CommService } from './common-services/comm.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'AynFrontend';

  constructor( private bnIdle: BnNgIdleService,
    public commservice:CommService,
    // public http:HttpClients
    ) { this.bnIdle.startWatching(300).subscribe((res) => {
      if (res) {
        if (localStorage.getItem('currentUser')) {
          this.commservice.logout();
        }
      }
    });}

  ngOnInit() {    
    // this.firstCall();
  }

}
