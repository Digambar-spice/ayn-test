import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import Swal from 'sweetalert2';
import { CommService } from '../common-services/comm.service';
import { Router } from '@angular/router';
import * as cloneDeep from 'lodash/cloneDeep';
// import swal from 'sweetalert';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  userForm: any;

  constructor(private formBuilder: FormBuilder,
    public commservice:CommService,
    public router:Router) { }

  ngOnInit() {
    this.userForm = this.formBuilder.group({
      Mobile: [''],
      Password: [''],
      Email: [''],
      ConfirmPassword: [''],
    });

    this.userForm = this.formBuilder.group({
      FirstName: ['',],
      LastName: ['',],
      Email: ['', [Validators.required, Validators.email]],
      Mobile:['', [Validators.required,Validators.pattern('^(\\+?\d{1,4}[\s-])?(?!0+\s+,?$)\\d{10}\s*,?$')]],
      Password:['',[Validators.required]],
      ConfirmPassword:['',[Validators.required]]
    });
  }
  onSubmit(){
    if(this.userForm.valid){
      if (this.userForm.value['Password'] !== this.userForm.value['ConfirmPassword']) {
        Swal.fire('', 'Password and confirm password does not match', 'error');
      }
      const regData = this.userForm.value;
      this.commservice.signUp(regData).subscribe((res:any) => {
        if(res){
          // const abc = res.json();
          let resp = cloneDeep(res);
          console.log(resp);
          
          if(resp.Success == true){
            Swal.fire('',resp.Message,'success');
            // sessionStorage.setItem('token',resp.Payload[0].token);
            // Swal.fire('','Login Successful','success')
            this.router.navigate(['/login']);
          }
          else{
            Swal.fire('','Invalid data for registration','error')
          }
        }
      });
      
    } else {
      Swal.fire('', 'Registration form is not valid', 'error');
    }
  }

}
