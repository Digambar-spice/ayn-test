import { Injectable } from '@angular/core';
import { HttpClients } from '../Core/httpClient';
import { Http } from '@angular/http';
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class CommService {

  headers: any;
  headerOption: any;
  ipAddress: any;

  constructor(
    // private http:HttpClients, 
    public httpLogin:Http,
    private _http: HttpClients,
    public router:Router,
    ) { 
    // this._http.get<{ ip: string }>('https://jsonip.com').subscribe(data => {
    //   this.ipAddress = data
    // })
  }

  loginUrl = 'login';
  signUrl = 'sign-up';
  fileUploadUrl = 'upload';
  refreshTokenUrl = 'refreshToken';


  login(logindata: any) { 
    return this._http.post(this.loginUrl, logindata);
  }

  upload(data: any) {
    return this._http.Post(this.fileUploadUrl, data);

  }

  signUp(signUpdata: any) {
    return this._http.post(this.signUrl, signUpdata);
  }

  getAuthorizationToken(){
    return this._http.post(this.refreshTokenUrl, '');
  }

  logout() {
    // remove user from local storage to log user out
    this.router.navigate(['/login']);
    localStorage.removeItem('currentUser');
    sessionStorage.clear();
  }

}
