import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { CommService } from '../common-services/comm.service';
import * as cloneDeep from 'lodash/cloneDeep';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { HttpClients } from '../Core/httpClient';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  ipAddress: any;
  constructor(public commservice:CommService,
    public router:Router,
    private _http: HttpClients
    ) {
    this.loginForm = new FormGroup({
      Email: new FormControl('', [Validators.required, Validators.email]),
      Password: new FormControl('', [Validators.required])
    });
    this._http.get<{ ip: string }>('https://jsonip.com').subscribe(data => {
      this.ipAddress = data
    })
   }

  ngOnInit() {
  }
  
  onSubmit() {
    if(this.loginForm.valid) {
      // console.log(this._v());
    }
  }

  _v() {
    return this.loginForm.value;
  }

  signUp(){
    this.router.navigate(['/register']);
  }


  login(dataObj){   
    
    if(this.loginForm.valid) {
    const credential = this.loginForm.value;
    credential.ip = this.ipAddress
    
    this.commservice.login(credential).subscribe((res:any) => {
      if(res){
        // const abc = res.json();
        let resp = cloneDeep(res);
        console.log(resp);
        
        if(resp.Success == true){
          
          sessionStorage.setItem('token',resp.Payload[0].token.Token);
          // sessionStorage.setItem('refreshToken',resp.Payload[0].token.RefreshToken);
          localStorage.setItem('currentUser', JSON.stringify(resp.Payload[0].token.Token));
          // Swal.fire('','Login Successful','success')
          Swal.fire('',resp.Message,'success');
          this.router.navigate(['/upload']);
        }
        else{
          Swal.fire('','Invalid User Name or Password','error')
        }
      }
    });
  }
}

}
