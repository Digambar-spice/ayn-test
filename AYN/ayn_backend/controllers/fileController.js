var multer = require('multer');
var ObjectID = require('mongodb').ObjectID;
var token = require('../common/key');
var mongoo = require('../models/model');
const mongodb = require('mongodb');
const MongoClient = require('mongodb');
var conf = require('../config')

var addurl = {}
var tempUrl = conf.URL.split('/');
tempUrl = tempUrl[tempUrl.length - 1];
if (tempUrl.length > 0) {
  addurl["url"] = conf.URL
  var Url = addurl.url
} else {
  addurl["url"] = conf.URL + conf.DB_A
  var Url = addurl.url
}

const storage = require('multer-gridfs-storage')({
  url: Url
  // url:'mongodb://localhost:27017/ActualDB'
});

var upload = multer({
  storage: storage
})

module.exports.fileUpload = function (req, res) {

  try {
    upload.array('file')(req, res, async function (err) {

      if (err) {
        res.send({ "Success": false, "Message": "Something went wrong, Please try again.", "Payload":[]});
        return;
      }

      res.send({ "Success": true, "Message": "Successfully saved", "Payload": [req.files] });
     

    });
  } catch (error) {
    res.send({ "Success": false, "Error": error.toString(), "Payload": [] });
  }

}


module.exports.replaceFile = function (req, res) {
  try {
    var fileId = req.body.fileId;
    MongoClient.connect(url, function (err, client) {
      if (err) {
        return res.json(
          {
            title: 'Download Error',
            message: 'MongoClient Connection error', error: err.errMsg
          });
      }
      const db = client.db(conf.DB_A);
      const collection = db.collection('fs.files');
      collection.deleteOne({ '_id': new ObjectID(fileId) }, async function (err, isDelete) {
        if (err) {
          res.send({
            message: 'error in delete'
          });
        }

        if (isDelete) {
          res.send({ "Success": true, "Message": "Successfully deleted", });
        }
      })
    });
  } catch (e) {
    res.send({ err: e.toString() })
  }
}

module.exports.getFile = (req, res) => {
  let fileName = new ObjectID(req.body.fileid);

  MongoClient.connect(url, function (err, client) {
    if (err) {
      res.json(
        {
          title: 'Download Error',
          message: 'MongoClient Connection error', error: err.errMsg
        });
    }
    const db = client.db(conf.DB_A);
    const collection = db.collection('fs.files');
    const collectionChunks = db.collection('fs.chunks');
    collection.find({ '_id': fileName }).toArray(function (err, docs) {
      if (err) {
        res.json({
          title: 'Download Error',
          message: 'Error finding file',
          error: err.errMsg
        });
      }
      if (!docs || docs.length === 0) {
        //   return res.json('index', {
        //    title: 'Download Error', 
        //    message: 'No file found'});   
        // res.status(err.status || 500);
        res.json({
          title: 'Download Error',
          message: 'No file found'
        });
      } else {

        //Retrieving the chunks from the db          
        collectionChunks.find({ files_id: docs[0]._id })
          .sort({ n: 1 }).toArray(function (err, chunks) {
            if (err) {
              res.json({
                title: 'Download Error',
                message: 'Error retrieving chunks',
                error: err.errmsg
              });
            }
            if (!chunks || chunks.length === 0) {
              //No data found            
              res.json({
                title: 'Download Error',
                message: 'No data found'
              });
            }

            if (chunks.length > 0) {
              let fileData = [];
              for (let i = 0; i < chunks.length; i++) {
                //This is in Binary JSON or BSON format, which is stored               
                //in fileData array in base64 endocoded string format               

                fileData.push(chunks[i].data.toString('base64'));
              }

              //Display the chunks using the data URI format          
              let finalFile = 'data:' + docs[0].contentType + ';base64,'
                + fileData.join('');
              res.json({
                docs: docs[0],
                title: 'Image File',
                message: 'Image loaded from MongoDB GridFS',
                imgurl: finalFile
              });
            }
          });
      }
    });
  });
};