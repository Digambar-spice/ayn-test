var express = require('express');
var User =require('../controllers/userController');
var file =require('../controllers/fileController');
var api = express.Router();

api.use('/v1/sign-up', User.save);
api.use('/v1/login', User.login);
api.use('/v1/refreshToken',User.refreshtoken)
api.use('/v1/upload',file.fileUpload);
api.use('/v1/download',file.getFile);
api.use('/v1/delete',file.replaceFile);

module.exports = api ;