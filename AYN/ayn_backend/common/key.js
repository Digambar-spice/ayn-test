
var crypto = require('crypto');
var nJwt = require('njwt')
var token_key = require('../config')
var mongo = require('../models/model');
var otpGenerator = require('otp-generator');
var ObjectID = require('mongodb').ObjectID;



class Keys{
    constructor(){
        this.users = 'Users'
    }
   


    async GenerateSalt() {
        var salt = crypto.randomBytes(Math.ceil(10 / 2))
        .toString('hex')
        .slice(0, 10);
        return salt
    }

    async HashPassword(userpassword,salt) {
        var hash = crypto.createHmac('md5' ,salt);
        hash.update(userpassword);  
        var value = hash.digest('hex');
        return value
    }

    async generatetoken (userid,tenantid,privatekey,publickey) {

        if(privatekey == null || publickey == null){
            let userdetails = await mongo.actual.collection(this.users).find({"UserID":new ObjectID(userid)},{fields:{"_id":0}}).toArray(); 
            privatekey = userdetails[0].PrivateKey,
            publickey = userdetails[0].PublicKey
        }
        var token_string = {
            userid: userid.toString(),
            privatekey:privatekey.toString(),
            publickey:publickey.toString()
        }
        var jwt = nJwt.create(token_string,token_key.token_key);
        jwt.setExpiration(new Date().getTime() + (28800000))
        var token = jwt.compact();
        //refrestokenu
        var res_token = {
            userid: userid.toString(),
            privatekey:privatekey.toString(),
            publickey:publickey.toString(),
        }
        var refreshToken = nJwt.create(res_token,token_key.token_key);
        var rToken = refreshToken.compact();
        
    
        return {"Token":token,"RefreshToken":rToken}
    }

    async decodetoken(res_token) {
    
        const token = res_token.replace("Bearer","")
        const base64Url = token.split('.')[1];
        const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        const buff = new Buffer.from(base64, 'base64');
        const payloadinit = buff.toString('ascii');
        const deoded_token = JSON.parse(payloadinit);
        var id = deoded_token.userid.toString()

        return id
    }

    async generatenumber(){
        try{
            var num = otpGenerator.generate(4, { digits: true, alphabets: false, specialChars: false, upperCase: false });
        var add_user = {"ID":num};
        let data = await mongo.ublock.collection(this.users).insertOne(add_user)
        let userid = await mongo.ublock.collection(this.users).find({"ID":num},{fields:{"_id":1}}).toArray();
        return (userid[0]._id.toString());
        }catch(e){
            return ({ err: e.toString() });
        }
    }

}


module.exports = Keys

