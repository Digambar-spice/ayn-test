var fs              = require('fs'),
    winston         = require('winston'),
    rotation        = require('winston-daily-rotate-file'),
    path            = require('path'),
    logConfig       = require('../share/logConfig');
            

var transports      = [];


// Creating logs directory
var logDirectory =  logConfig.directory;
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);

winston.transports.DailyRotateFile = rotation;


transports.push(new winston.transports.DailyRotateFile({
    name: 'file',
    datePattern: logConfig.datePattern,   //Date wise logs
    filename: path.join(logConfig.directory, logConfig.filename)
}));





let logger = winston.createLogger({
    transports: transports
});


module.exports = logger;