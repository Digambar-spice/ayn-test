var mongo = require('../models/model');
var key = require('../common/key');
var blockchainkey = require('bigchaindb-driver');
var ObjectID = require('mongodb').ObjectID;



class Auth {
    constructor() {
        this.users = 'Users';
        this.filters = 'Filters';
        this.otpvalidation = 'OtpValidation';
    }

//register
    async register(req,jsonObj) {
    try {
        var bodyInfo = req;
            var filter = {}
            filter["$or"]=[]
            if (jsonObj.Email.toString() != null){
                filter["$or"].push({Email:{'$regex' :"^"+jsonObj.Email.toString(), '$options' : 'i'},TenantID:bodyInfo.TenantData.TenantID});
            };
            if (jsonObj.Mobile != null){
                filter["$or"].push({PhoneNumber:parseInt(jsonObj.Mobile),TenantID:bodyInfo.TenantData.TenantID});
            };
            let data = await mongo.actual.collection(this.users).find(filter).toArray();
                if (data.length == 0){
                    var salt =await new key().GenerateSalt() // generate Salt
                    var Password_hash =await new key().HashPassword(jsonObj.Password, salt) //generate hash password
                    var blockkey = new blockchainkey.Ed25519Keypair() // generate Private Key and
                    var userid =await new key().generatenumber()            
                    var add_user = {
                        UserID: new ObjectID(userid),
                        Password: Password_hash,
                        Salt: salt,
                        PrivateKey : blockkey.privateKey,
                        PublicKey : blockkey.publicKey,
                        IsValid : false,
                        DialCode : "091",
                        ModifiedDate:new Date()
                    };
                    if(jsonObj.Email.toString() != null && jsonObj.Mobile != null){
                        add_user['Email'] =  jsonObj.Email
                        add_user['EmailValid'] = false
                        add_user['MobileValid'] =  false
                        add_user['PhoneNumber'] = parseInt(jsonObj.Mobile,10)
                        var reqdata=  [{"Mobile":jsonObj.Mobile,"Email":jsonObj.Email}]
                        var usercontact = parseInt(jsonObj.Mobile)
                    }
                    else if(jsonObj.Email != null){
                        add_user['Email'] = jsonObj.Email
                        add_user['EmailValid'] = false
                        var usercontact = jsonObj.Email
                        var reqdata=  [{"Mobile":jsonObj.Email}]

                    
                    } 
                    else if(jsonObj.Mobile != null ){
                        add_user['PhoneNumber'] =  parseInt(jsonObj.Mobile,10)
                        var MessageID = 4
                        add_user['MobileValid'] =  false
                        var reqdata=  [{"TenantID":bodyInfo.TenantData.TenantID,"Mobile":jsonObj.Mobile}]
                        

                        var usercontact = parseInt(jsonObj.Mobile)
                    }
                   
                    await  mongo.actual.collection(this.users).insertOne(add_user);//insert userdata
                    return ({"Success":true,"Message":'User registered',
                            "Payload":[{"UserContact":usercontact}]});
                }else{


                    if(data.Password == undefined){
                        // let mesg = await new msg().getmessage(bodyInfo.TenantData.TenantID,bodyInfo.TenantData.ModuleID,13)
                        return ({ 'Success': false, 'Error': '',"IsEmpExit":true});

                    }
                    // let mesg = await new msg().getmessage(bodyInfo.TenantData.TenantID,bodyInfo.TenantData.ModuleID,1)
                    return {"Success":false,"Error":'',"Payload":[]}
                }
    }catch(e){
        return { "Success":false,"Error": e.toString(),"Payload":[] }
    }
};
}

module.exports = Auth