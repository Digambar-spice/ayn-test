var ObjectID = require('mongodb').ObjectID;
var mongo = require('./model');
var Credetials = require('../config')
var key = require('../common/key');
var blockchainkey = require('bigchaindb-driver');
var ObjectID = require('mongodb').ObjectID;
var wlogger = require('../common/logger');
const logger = require('../common/logger');

class save{
    constructor(){
        this.user = "Users";
    }

    async savedata(bodyInfo){

        try {
            var filter = {}
            filter["$or"]=[]
            if (bodyInfo.Email.toString() != null){
                filter["$or"].push({Email:{'$regex' :"^"+bodyInfo.Email.toString(), '$options' : 'i'}});
            }
            if (bodyInfo.Mobile != null){
                filter["$or"].push({Mobile:parseInt(bodyInfo.Mobile)});
            }
            let data = await mongo.actual.collection(this.user).find(filter).toArray();
            if(data.length == 0){
            var salt =await new key().GenerateSalt() // generate Salt
            var Password_hash =await new key().HashPassword(bodyInfo.Password, salt) //generate hash password
            var blockkey = new blockchainkey.Ed25519Keypair() // generate Private Key and
            var userid =await new key().generatenumber()     
            var insertdata = { "UserID": new ObjectID(userid), 
                               "FirstName":bodyInfo.FirstName,
                               "LastName":bodyInfo.LastName,
                               "Email": bodyInfo.Email,
                               "salt" :salt,
                               "Password": Password_hash,
                               "Mobile": parseInt(bodyInfo.Mobile,10),
                               "PrivateKey" : blockkey.privateKey,
                               "PublicKey" : blockkey.publicKey,
                               "AddedDateTime": new Date(Date.now()).toISOString()}
            await mongo.actual.collection(this.user).insertOne(insertdata);

            return { "Success": true, "Message":  "User successfully registered ", "Payload": [] }
        }   
        else{

            
            return {"Success":false,"Error":"Email or Mobile Number is already registered","Payload":[]}

        }

                    
        } catch (error) {
            return { "Success": false, "Error": error.toString(), "Payload": [] } 
        }
    }


    //login
    async login(bodyInfo){
        try {
            // LOGIN WITH PASSWORD
                    var filter ={}
                    filter["$and"]=[]
                    if (bodyInfo.Email.toString() != null){

                       
                        filter["$and"].push({Email:{'$regex' : "^"+bodyInfo.Email.toString(), '$options' : 'i'}});
                    }
                    // else if (bodyInfo.Mobile != null){
                    //     filter["$and"].push({ Mobile:bodyInfo.Mobile});
                    // }
                    let record = await mongo.actual.collection(this.user).findOne(filter);
                    if(record){
                        if(record.Email == bodyInfo.Email || record.Mobile == bodyInfo.Mobile){
                            var passwordhash = record.Password
                            var Password_hash =await new key().HashPassword(bodyInfo.Password, record.salt);
                            if(passwordhash == Password_hash){
                                var token =await new key().generatetoken(record.UserID,record.PrivateKey,record.PublicKey)
                                logger.info({userID:record.UserID , ipAddress: bodyInfo.ip.ip,  action: '/Login', responseCode: '200', message: '/User logged in successfully'});
                                return ({ 'Success': true, 'Message': "User logged in successfully", 'Payload':[{'token':token}]});
                            }else{
                                return ({ 'Success': false, 'Message': "Password does not match", 'Payload':[]});
                            }
                        }else{
                            return ({ 'Success': false, 'Message': "Email or Mobile is not valid", 'Payload':[]});     
                        }
                    }else{
                        return ({ 'Success': false, 'Message': " Record is not avialable for given email or mobile", 'Payload':[]});
                    }
    }
     catch (e) {
        return ({"Success":false,"Error":e.toString(),"Payload":[]})     
    }
    };
}


module.exports = save;