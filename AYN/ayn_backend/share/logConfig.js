var path = require('path');

var logDirectory = path.join(__dirname, '../logs/');

module.exports = {
    directory: logDirectory,
    filename: "log_file.log",
    datePattern: '.yyyy-MM-dd',
    logLevel: 'error'
};