var CryptoJS = require("crypto-js");

// function checkValidation(fieldValue, fieldObj){
//     arr = [];    
//     if(fieldObj["SubField"] != undefined && fieldObj.SubField.length > 0){
//                 for(var j =0;j<fieldObj.SubField.length;j++){
//                     for(var k =0;k<fieldObj.SubField[j].validations.length;k++){
//                         var pattern = new RegExp(fieldObj.SubField[j].Validate[k].Pattern)
//                         var Message = fieldObj.SubField[j].Validate[k].Message
//                         if (fieldObj.SubField[j].Validate[k].ValidationID === 1) {
//                             if (fieldValue == null || fieldValue == '' || fieldValue == "" || fieldValue == undefined) {
//                                      arr.push( Message);   
//                             }
//                         } else {
//                         if (pattern.test(fieldValue) == false && fieldValue != '' && fieldValue != null) {
//                             if(arr.includes(Message) == false){
                                
//                                 arr.push(Message);
//                             }
//                         }
//                     } 
//                     }
//                 }
//             }
          
//     if( fieldObj.Validate &&  fieldObj.Validate.length >0){
//             fieldObj.Validate.forEach(val => {
//             const pattern =  new RegExp(val.Pattern);
//             const Message = val.Message;
//             const ID = val.ValidationID;

//             if (ID == 1 || ID == 7) {
//                 if (fieldValue == null || fieldValue == '' || fieldValue == "" || fieldValue == undefined ) {
//                     if(fieldObj.Validate.length > 0) {
//                         return arr.push(Message);
//                     }else {
//                          arr.push( Message);
//                     }
//                 }
//             } else {
//                 if (pattern.test(fieldValue) === false && fieldValue != '' && fieldValue != null) {
//                     arr.push(Message);
//                 }
//             }  
             
//         });
//     }
//     if(fieldObj.RoleID){
//         return { field: fieldObj.name, error: arr ,fieldRoleId: fieldObj.RoleID};
//     }else{
//         return { field: fieldObj.name, error: arr,fieldRoleId:''};
//     }

// }
function encryptData(data,tokenkey){
    try{
    var strenc = CryptoJS.AES.encrypt(JSON.stringify(data),tokenkey).toString();
    // return {"data": strenc};
    return strenc

    }catch(e){
    console.log(e);
    }
    }
    
    function decryptData(data,tokenkey){
    try{
    var bytes = CryptoJS.AES.decrypt(data,tokenkey)
    var originalText = bytes.toString(CryptoJS.enc.Utf8);

    return JSON.parse(originalText);
    }catch(e){
    console.log(e);
    }
    }

module.exports = {
    // checkValidation: checkValidation,
    encryptData: encryptData,
    decryptData: decryptData 

}
